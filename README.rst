.. image:: https://img.shields.io/pypi/v/ssh2-python.svg?label=ssh2-python&style=plastic
   :target: https://badge.fury.io/py/ssh2-python
   :alt: ssh2-python
.. image:: https://img.shields.io/pypi/v/netssh2.svg?style=plastic
   :target: https://pypi.python.org/pypi/ssh2-python
   :alt: PyPI
.. image:: https://img.shields.io/pypi/l/netssh2.svg?label=License&style=plastic
   :alt: PyPI - License
   :target: https://gitlab.com/jkrysl/netssh2/blob/master/LICENSE
.. image:: https://img.shields.io/pypi/pyversions/ssh2-python.svg?style=plastic
   :target: https://pypi.python.org/pypi/ssh2-python
.. image:: https://img.shields.io/pypi/wheel/netssh2.svg?style=plastic
   :alt: PyPI - Wheel
   :target: https://pypi.python.org/pypi/ssh2-python
.. image:: https://img.shields.io/gitlab/pipeline/jkrysl/netssh2.svg?style=plastic
   :alt: Gitlab pipeline status
   :target: https://badge.fury.io/py/ssh2-python
.. image:: https://img.shields.io/readthedocs/netssh2.svg?style=plastic
   :alt: Read the Docs
   :target: https://netssh2.readthedocs.io/en/latest/

Project description
===================

Library to use ssh2-python to communicate with different network devices.

Installation
============

    pip install netssh2


Documentation
=============
Documentation can be found here: https://netssh2.readthedocs.io/en/latest/
