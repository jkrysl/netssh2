Installation
============

The recommended installation method is using `pip`.


.. code-block:: shell

    pip install netssh2


Installation from source
========================


.. code-block:: shell

    git clone https://gitlab.com/jkrysl/netssh2
    cd netssh2
    python setup.py install