About NetSSH2
=============

The goals of this project are to create alternative to `netmiko`_ by replacing `Paramiko` with `ssh2-python`_.

As `ssh-python` is designed to be faster than `Paramiko`, so should this library.

Contributions are most welcome!

.. _netmiko: https://github.com/ktbyers/netmiko
.. _ssh2-python: https://github.com/ParallelSSH/ssh2-python