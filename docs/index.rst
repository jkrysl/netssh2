NetSSH2 Documentation
=====================

 Library for communicating with network devices using `ssh2-python`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   installation
   api
   changelog_link


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _ssh2-python: https://github.com/ParallelSSH/ssh2-python
